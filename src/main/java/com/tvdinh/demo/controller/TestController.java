package com.tvdinh.demo.controller;

import com.tvdinh.demo.annotation.LoginKaptcha;
import com.tvdinh.demo.domain.AuthToken;
import com.tvdinh.demo.domain.LoginRequest;
import com.tvdinh.demo.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class TestController {

    private final UserService userService;

    @LoginKaptcha
    @PostMapping("/authenticate")
    public ResponseEntity<AuthToken> login(@RequestBody LoginRequest loginRequest, HttpServletRequest request, HttpServletResponse response) {
        return ResponseEntity.ok(userService.login(loginRequest, request, response));
    }
}
