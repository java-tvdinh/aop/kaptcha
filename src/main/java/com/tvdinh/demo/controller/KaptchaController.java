package com.tvdinh.demo.controller;

import com.tvdinh.demo.domain.CaptchaDTO;
import com.tvdinh.demo.repository.KaptchaRepository;
import com.tvdinh.demo.service.KaptchaService;
import com.tvdinh.demo.ultis.BadRequestError;
import com.tvdinh.demo.ultis.ResponseException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.Objects;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class KaptchaController {
    private final KaptchaService kaptchaService;
    private final KaptchaRepository kaptchaRepository;

    @GetMapping("/public/captcha")
    public ResponseEntity<CaptchaDTO> getCaptcha(HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        return ResponseEntity.ok().body(kaptchaService.generate());
    }

    /**
     * Test get and delete cache
     */
    @PostMapping(value = "/public/captcha", consumes = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<CaptchaDTO> getCaptchaByID(@RequestBody String transactionId, HttpServletRequest request, HttpServletResponse response) {
        String captcha = kaptchaRepository.getIfPresent(transactionId);
        CaptchaDTO captchaDTO = null;
        if (Objects.nonNull(captcha) || kaptchaService.validate(transactionId, captcha)) {
            captchaDTO = new CaptchaDTO(transactionId, captcha);
        } else {
            //Sinh mới captcha khi nhập sai trả về cho ui nhập lại
            Map<String, Object> params = kaptchaService.generateRequired();
            throw new ResponseException("Incorrect captcha", BadRequestError.INCORRECT_CAPTCHA, params);
        }

        return ResponseEntity.ok().body(captchaDTO);
    }
}
