package com.tvdinh.demo.aop;

import com.tvdinh.demo.config.KaptchaProperties;
import com.tvdinh.demo.service.KaptchaService;
import com.tvdinh.demo.service.LoginAttemptService;
import com.tvdinh.demo.ultis.BadRequestError;
import com.tvdinh.demo.ultis.HttpUtil;
import com.tvdinh.demo.ultis.ResponseException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.Objects;

@Slf4j
@Aspect
@Component
@RequiredArgsConstructor
public class KaptchaAspect {

    private final KaptchaProperties kaptchaProperties;
    private final KaptchaService kaptchaService;
    private final LoginAttemptService loginAttemptService;
    private final PasswordEncoder encoder;

    @Before("@annotation(com.tvdinh.demo.annotation.RequiredKaptcha)")
    public void requiredKaptcha() throws Throwable {
        String headerName = kaptchaProperties.getHeaderName();

        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
                .getRequest();

        String captcha = request.getHeader(headerName);
        String transactionId = request.getHeader("X-TRANSACTION-ID");

        log.info("captcha: " + captcha);
        log.info("transactionId: " + transactionId);

        //Kiểm tra captcha nhập với tranId(được sinh từ captcha) và xóa cache nếu tồn tại
        if (Objects.isNull(captcha) || Objects.isNull(transactionId)
                || !encoder.matches(captcha, transactionId)
                || !kaptchaService.validate(transactionId, captcha)
        ) {
            throw new RuntimeException("Null value kaptcha");
        }
    }

    @Before("@annotation(com.tvdinh.demo.annotation.LoginKaptcha)")
    public void loginKaptcha() throws Throwable {
        String headerName = kaptchaProperties.getHeaderName();

        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
                .getRequest();

        // check nếu ip đang không bị block thì bỏ qua(attempt<loginMaxAttemptTime)
        if (!loginAttemptService.isRequiredCaptcha(HttpUtil.getClientIP(request))) {
            return;
        }

        String captcha = request.getHeader(headerName);
        String transactionId = request.getHeader("X-TRANSACTION-ID");

        log.info("captcha: " + captcha);
        log.info("transactionId: " + transactionId);

        //Kiểm tra captcha nhập với tranId(được sinh từ captcha) và xóa cache nếu tồn tại
        if (Objects.isNull(captcha) || Objects.isNull(transactionId)
                || !encoder.matches(captcha, transactionId)
                || !kaptchaService.validate(transactionId, captcha)
        ) {
            //Sinh mới captcha khi nhập sai trả về cho ui nhập lại
            Map<String, Object> params = kaptchaService.generateRequired();
            throw new ResponseException("Incorrect captcha", BadRequestError.INCORRECT_CAPTCHA, params);
        }
    }

}
