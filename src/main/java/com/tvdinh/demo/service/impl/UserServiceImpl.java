package com.tvdinh.demo.service.impl;

import com.tvdinh.demo.domain.AuthToken;
import com.tvdinh.demo.domain.LoginRequest;
import com.tvdinh.demo.service.KaptchaService;
import com.tvdinh.demo.service.LoginAttemptService;
import com.tvdinh.demo.service.UserService;
import com.tvdinh.demo.ultis.BadRequestError;
import com.tvdinh.demo.ultis.HttpUtil;
import com.tvdinh.demo.ultis.ResponseException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final KaptchaService kaptchaService;
    private final LoginAttemptService loginAttemptService;

    @Override
    public AuthToken login(LoginRequest loginRequest, HttpServletRequest request, HttpServletResponse response) {
        String ip = HttpUtil.getClientIP(request);
        try {
            if (!Objects.equals(loginRequest.getUsername(), "admin")) {
                throw new RuntimeException();
            }
            // clear login failed attempt
            this.loginAttemptService.loginSucceeded(ip);
            return AuthToken.builder().build();
        } catch (Exception e) {
            loginAttemptService.loginFailed(ip);
            Map<String, Object> params = loginAttemptService.isRequiredCaptcha(ip) ? kaptchaService.generateRequired() : new HashMap<>();
            throw new ResponseException("Invalid username password", BadRequestError.INVALID_USERNAME_PASSWORD, params);
        }
    }
}
