package com.tvdinh.demo.service.impl;

import com.google.code.kaptcha.Producer;
import com.tvdinh.demo.domain.CaptchaDTO;
import com.tvdinh.demo.repository.KaptchaRepository;
import com.tvdinh.demo.service.KaptchaService;
import com.tvdinh.demo.ultis.FileUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Service
@Slf4j
public class KaptchaServiceImpl implements KaptchaService {

    private final Producer kaptchaProducer;
    private final PasswordEncoder encoder;
    private final KaptchaRepository kaptchaRepository;

    public KaptchaServiceImpl(Producer captchaProducer, PasswordEncoder encoder, KaptchaRepository kaptchaRepository) {
        kaptchaProducer = captchaProducer;
        this.encoder = encoder;
        this.kaptchaRepository = kaptchaRepository;
    }

    @Override
    public CaptchaDTO generate() {
        String capText = kaptchaProducer.createText();

        String transactionId = this.encoder.encode(capText);

        kaptchaRepository.put(transactionId, capText);
        // create the image with the text
        BufferedImage bi = kaptchaProducer.createImage(capText);

        return new CaptchaDTO(transactionId, FileUtil.getImageSrcBase64String(bi, "jpg"));
    }

    @Override
    public Map<String, Object> generateRequired() {
        String capText = kaptchaProducer.createText();

        String transactionId = this.encoder.encode(capText);

        kaptchaRepository.put(transactionId, capText);
        // create the image with the text
        BufferedImage bi = kaptchaProducer.createImage(capText);

        Map<String, Object> data = new HashMap<>();

        data.put("captcha", FileUtil.getImageSrcBase64String(bi, "jpg"));
        data.put("transactionId", transactionId);
        data.put("captchaRequired", true);

        return data;
    }

    @Override
    public boolean validate(String transactionId, String text) {
        String textInCache = kaptchaRepository.getIfPresent(transactionId);
        if (Objects.nonNull(textInCache) && textInCache.equals(text)) {
            // invalidate captcha - xóa cache
            kaptchaRepository.invalidate(transactionId);
            return true;
        }
        return false;
    }
}
