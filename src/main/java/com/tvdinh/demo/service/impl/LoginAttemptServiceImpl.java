package com.tvdinh.demo.service.impl;

import com.tvdinh.demo.repository.LoginFailedRepository;
import com.tvdinh.demo.service.LoginAttemptService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Slf4j
@Service
@RequiredArgsConstructor
public class LoginAttemptServiceImpl implements LoginAttemptService {

    private final LoginFailedRepository loginFailedRepository;
    @Value("${login.max-attempt-time}")
    private int loginMaxAttemptTime;

    @Override
    public void loginSucceeded(String key) {
        this.loginFailedRepository.invalidate(key);
    }

    private int getAttempts(String key) {
        Integer attempts = this.loginFailedRepository.getIfPresent(key);

        return Objects.nonNull(attempts) ? attempts : 0;
    }

    @Override
    public void loginFailed(String key) {
        int attempts = this.getAttempts(key);

        attempts++;

        log.warn("User with ip {} login failure for {} times", key, attempts);

        this.loginFailedRepository.put(key, attempts);
    }

    @Override
    public boolean isRequiredCaptcha(final String key) {
        return this.getAttempts(key) >= loginMaxAttemptTime;
    }
}
