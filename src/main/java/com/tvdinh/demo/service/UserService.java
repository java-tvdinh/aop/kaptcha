package com.tvdinh.demo.service;

import com.tvdinh.demo.domain.AuthToken;
import com.tvdinh.demo.domain.LoginRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface UserService {

    AuthToken login(LoginRequest loginRequest, HttpServletRequest request, HttpServletResponse response);

}
