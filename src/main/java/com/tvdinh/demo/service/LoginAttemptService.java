package com.tvdinh.demo.service;

public interface LoginAttemptService {
	void loginSucceeded(final String key);
	
	void loginFailed(final String key);
	
	boolean isRequiredCaptcha(final String key);
}
