package com.tvdinh.demo.service;

import com.tvdinh.demo.domain.CaptchaDTO;

import java.util.Map;

public interface KaptchaService {
    CaptchaDTO generate();

    Map<String, Object> generateRequired();

    boolean validate(String transactionId, String text);
}
