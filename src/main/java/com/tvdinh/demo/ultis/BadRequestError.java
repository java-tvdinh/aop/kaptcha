package com.tvdinh.demo.ultis;

import lombok.Getter;

@Getter
public enum BadRequestError implements ResponseError {

    INCORRECT_CAPTCHA(400, "Incorrect captcha"),
    INVALID_USERNAME_PASSWORD(401, "Invalid username password");

    private final Integer code;
    private final String message;

    BadRequestError(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String getName() {
        return name();
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public int getStatus() {
        return 400;
    }

    @Override
    public Integer getCode() {
        return code;
    }

}
