package com.tvdinh.demo.ultis;

import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.codec.binary.Base64;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.sql.Blob;
import java.sql.SQLException;

@Slf4j
public class FileUtil {

    public static BufferedImage decodeToImage(String imageString) {
        BufferedImage image = null;

        byte[] imageByte;

        try {
            imageByte = Base64.decodeBase64(imageString);

            ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);

            image = ImageIO.read(bis);

            bis.close();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return image;
    }

    public static String getImageSrcBase64String(BufferedImage image, String type) {
        StringBuilder sb = new StringBuilder();

        sb.append("data:image");
        sb.append("/");
        sb.append(type);
        sb.append(";base64,");
        sb.append(encodeToString(image, type));

        return sb.toString();
    }

    public static String encodeToString(BufferedImage image, String type) {
        String imgStr = "";

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        try {
            ImageIO.write(image, type, bos);

            byte[] imageBytes = bos.toByteArray();

            imgStr = Base64.encodeBase64String(imageBytes);

            bos.close();
        } catch (IOException ioe) {
            log.error(ioe.getMessage(), ioe);
        }

        return imgStr;
    }

    public static String getImageBase64String(Blob blob, String type) {
        String imgStr = "";

        try {
            InputStream inStream = blob.getBinaryStream();

            imgStr = getImageBase64String(inStream, type);
        } catch (SQLException sqlex) {
            log.error(sqlex.getMessage(), sqlex);
        }

        return imgStr;
    }

    public static String getImageBase64String(InputStream inStream, String type) {
        String imgStr = "";

        try {
            BufferedImage img = ImageIO.read(inStream);

            imgStr = encodeToString(img, type);
        } catch (IOException ioe) {
            log.error(ioe.getMessage(), ioe);
        }

        return imgStr;
    }

    public static String getImageBase64String(String url, String type) {
        String imgStr = "";

        try {
            BufferedImage img = ImageIO.read(new File(url));

            imgStr = encodeToString(img, type);
        } catch (IOException ioe) {
            log.error(ioe.getMessage(), ioe);
        }

        return imgStr;
    }
}
