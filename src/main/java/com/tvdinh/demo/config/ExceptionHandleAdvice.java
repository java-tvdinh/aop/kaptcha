package com.tvdinh.demo.config;

import com.tvdinh.demo.domain.error.ErrorResponse;
import com.tvdinh.demo.ultis.ResponseError;
import com.tvdinh.demo.ultis.ResponseException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
@Slf4j
public class ExceptionHandleAdvice {

    @ExceptionHandler(ResponseException.class)
    public ResponseEntity<ErrorResponse<Object>> handleResponseException(ResponseException e, HttpServletRequest request) {
        log.warn("Failed to handle request {}: {}", request.getRequestURI(), e);
        ResponseError error = e.getError();
        return ResponseEntity.status(error.getStatus())
                .body(ErrorResponse.<Object>builder()
                        .code(error.getCode())
                        .error(error.getName())
                        .message("")
                        .data(e.getParams())
                        .build());
    }

}
