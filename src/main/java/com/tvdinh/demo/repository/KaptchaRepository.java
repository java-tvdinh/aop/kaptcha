package com.tvdinh.demo.repository;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;

public interface KaptchaRepository {
    /**
     * Use redis cache
     */

    //Put value to cache - key
    @CachePut(cacheNames = "captcha", key = "#key", unless = "#result == null")
    default String put(String key, String data) {
        return data;
    }

    //Get value if exist cache - key . case key not found return null
    @Cacheable(cacheNames = "captcha", key = "#key", unless = "#result == null")
    default String getIfPresent(String key) {
        return null;
    }

    //Delete cache - key
    @CacheEvict(cacheNames = "captcha", key = "#key")
    default String invalidate(String key) {
        return key;
    }
}
