package com.tvdinh.demo.repository;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;

public interface LoginFailedRepository {

    @CachePut(cacheNames = "login-failed", key = "#key", unless = "#result == null")
    default Integer put(String key, Integer value) {
        return value;
    }

    @Cacheable(cacheNames = "login-failed", key = "#key", unless = "#result == null")
    default Integer getIfPresent(String key) {
        return null;
    }

    @CacheEvict(cacheNames = "login-failed", key = "#key")
    default String invalidate(String key) {
        return key;
    }

}
